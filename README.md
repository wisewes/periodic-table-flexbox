# Periodic Table of Elements structured with Flexbox

Although not interactive, this simple project uses CSS3 Flexbox for structuring the data.  The website for Los Alamos National Laboratory structures the periodic table using an HTML table. I decided to restructure it with Flexbox.

